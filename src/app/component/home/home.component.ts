import {Component, OnInit} from '@angular/core';
import {EventbriteService} from '../../service/eventbrite.service';
import {switchAll} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private eventBrite: EventbriteService) {
  }

  eventsList: Array<any>;
  eventHolder: any;

  ngOnInit() {
  }

  getEvents(keyword: string) {
    Swal.fire({
      allowOutsideClick: false,
      type: 'info',
      text: 'Wait please'
    });
    Swal.showLoading();

    this.eventBrite.getEvents(keyword).subscribe(res => {
      this.eventBrite.recordLog(keyword).subscribe(r => console.log(r));
      this.eventsList = res;
      Swal.close();
    });
  }

  sendEvent(item: any) {
    Swal.fire({
      allowOutsideClick: false,
      type: 'warning',
      text: 'The event has been added',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, added it!',
    });

    this.eventBrite.addToCalendar(item).subscribe(res => console.log(res));
    this.eventBrite.sendWhatsapp().subscribe( r => console.log('Message sent'));
  }

  serchForEvents(keyword: string) {
    console.log(keyword);
    this.getEvents(keyword);

  }
}
