import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventbriteService {

  API_URL = 'http://localhost:8080';

  constructor(private  httpClient: HttpClient) {}

  getEvents(keyword: string): Observable<any> {
    return this.httpClient.get(this.API_URL + '/events/' + keyword);
  }

  addToCalendar(item: any): Observable<any> {
    return this.httpClient.post(this.API_URL + '/concierge/google/event', item);
  }

  sendWhatsapp(): Observable<any> {
    return this.httpClient.get(this.API_URL + '/whatsapp/send');
  }

 recordLog(keyword: string): Observable<any> {
   // tslint:disable-next-line:indent
 	return this.httpClient.get(this.API_URL + '/logs/getMessage/' + keyword);
 }

}
